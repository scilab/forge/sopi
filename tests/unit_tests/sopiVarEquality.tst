// Copyright 2015 - Pierre Vuillemin
//
// Date of creation: 18 nov. 2015
//
sopi_begin;
n = 4;
x = sopi_var(n);
A = rand(n,n);
B = A +rand(n,n); // B ~= A
b = rand(n,1);
//
z1 = A*x;
z2 = A*x;
z3 = B*x;
assert_checktrue(sopi_varAreEquals(z1,z2));
assert_checkfalse(sopi_varAreEquals(z1,z3));
z4 = A*x + b;
z5 = b + A*x;
z6 = A*x + b + b;
assert_checkfalse(sopi_varAreEquals(z4,z6));

