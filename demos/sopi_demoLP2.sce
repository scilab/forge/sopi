// Data
n           = 3;
W           = rand(n,n);
d           = rand(n,1);
xMax        = 5*rand(n,1);
A           = rand(2,n);
b           = 20*rand(2,1);
sopi_begin;                                 // Initialisation of the name space used by sopi
sopi_mode verbose 2
x           = sopi_var(n);                  // Creation of the optimisation variable
LB          = x >= 0;                       // Lower bound definition
UB          = x <= xMax;                    // Upper bound definition
ceq         = x(2) == 0.5*xMax(2);          // Some (linear) equality constraint
ci          = A*x <= b;                     // Some (linear) inequality constraints
fun         = norm(W*(x-d),1);              // The objective function
problem     = sopi_min(fun,LB,UB,ceq,ci);   // Optimisation problem creation
problem.solve();                            // Problem resolution
xopt2        = problem.getOptValue(x);       // extraction of the optimal value of x
problem.solve("sopiLP");                            // Problem resolution
xopt1        = problem.getOptValue(x);       // extraction of the optimal value of x
disp([xopt1 xopt2])
disp([norm(W*(xopt1-d),1) norm(W*(xopt2-d),1)])

//[cs, As, bs, Ts, ds] = sopi_lpToStandardForm(problem.c, problem.A, problem.b, problem.Aeq, problem.beq, problem.lb, problem.ub);
//
//[xoptk, fopt, flag] = karmarkar(full(As), full(bs), full(cs));
//disp(flag)
//[xopts, fopt, flag] = sopi_primalSimplex(cs, As, bs);
//disp(flag)
//disp([xoptk, xopts])
//disp([xopt2 <= xMax;...
//      abs(xopt2(2) - 0.5*xMax(2))<=1e-8;...
//      A*xopt2 <= b;...
//      xopt2 >= 0]);
//
//disp([xopt1 <= xMax;...
//      abs(xopt1(2) - 0.5*xMax(2))<=1e-9;...
//      A*xopt1 <= b;...
//      xopt1 >= 0]);
//
