// Copyright 2015 - Pierre Vuillemin
// License CECILL
//

// ============================================================================
// SOPI QP SOLVE
// ============================================================================

// sopi_solveQP ................................................................
// is the main interface for accessing sopi solvers for quadratic problems.
// It is aimed at solving the following quadratic problems :
// min  x' H x + c' x
//  x
// s.t. A x <= b
//      Aeq x = beq
//      lb <= x <= ub
// where H is symmetric (not necessarily positive semi-definite)
function [xopt,fopt] = sopi_qpSolve(H,c,A,b,Aeq,beq,lb,ub)
   // handle optional arguments
   // TODO:
   // Determine type of quadratic problem
   qpHasEC     = ~isempty(Aeq);
   qpHasIC     = ~isempty(A);
   qpHasLB     = ~isempty(lb) & or(lb ~=-%inf);
   qpHasUB     = ~isempty(ub) & or(ub ~= %inf);
   qpHasBounds = qpHasLB & qpHasUB;
   typeOfH     = sopi_testMatrixPositivity(H);
   qpIsConvex  = typeOfH == "pd" | typeOfH == "psd";
   //    qpIsConcave = typeOfH == "nd" | typeOfH == "nsd";
   //
   nvar = size(H,1);
   if isempty(c) then
      c = zeros(nvar,1);
   end

   if  qpIsConvex then
      if ~qpHasEC & ~qpHasBounds & ~qpHasIC then     // unconstrained convex case
         sopi_print(1,"Solving convex unconstrained quadratic problem.\n")
         [xopt, fopt] = sopi_solveConvexUQP(H,c,r);
      elseif qpHasEC & ~qpHasBounds & ~qpHasIC then  // equality constrained convex case
         sopi_print(1,"Solving convex equality constrained quadratic problem.\n")
         [xopt, fopt] = sopi_solveConvexECQP(H,c,r,Aeq,beq);
      else
         // TODO : general case : Active set,IP
         error("nope")
      end
   else
      error("nope")
   end
endfunction

// ============================================================================
// CONVEX QP
// ============================================================================

// sopi_solveConvexUQP ........................................................
// solves convex unconstrained quadratic problems.
function [xopt, fopt] = sopi_solveConvexUQP(H,c,r)
   xopt = -H\c;
   fopt = xopt'*H*xopt + c'*xopt + r;
endfunction

// sopi_solveECQP ..............................................................
// solves quadratic problems with linear equality constraints :
// min x'*H*x + c'*x + r s.t. A*x = b where H is symmetric
function [xopt, fopt] = sopi_solveECQP(H,c,r,A,b,method)
   // default method
   if argn(2)<6 then
      method = "full-space";
   end
   xopt        = [];
   fopt        = [];
   [m,n]       = size(A);
   select method
   case "full-space" // inversion of the KKT matrix
      kktMat      = [H,A';A,sparse([],[],[m,m])];
      rhs         = [-c;b];
      y           = kktMat\rhs;
      xopt        = y(1:n);
   case "elimination" // elimination of the equality constraints
      [Q,R]       = qr(A');
      R(m+1:$,:)  = [];
      R(:,m+1:$)  = []
      Q1          = Q(:,1:m);
      Q2          = Q(:,m+1:$);
      Yb          = Q1*(R'\b);
      Z           = Q2;
      y           = sopi_solveConvexUQP(Z'*H*Z,Z'*(c+H*Yb),0);//(Z'*H*Z)\(-Z'*(c+H*Yb));
      xopt        = Yb + Z*y;
   end
   fopt = xopt'*H*xopt + c'*xopt + r;
endfunction
