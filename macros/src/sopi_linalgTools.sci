// Copyright 2015 - Pierre Vuillemin
// License CECILL
//

// ============================================================================
// BASIC MATRIX TESTS
// ============================================================================

// sopi_isMatrix ...............................................................
// tests whether a given sopiVar is a matrix.
function isMatrix = sopi_isMatrix(var)
   [m,n]    = size(var);
   isMatrix = and([m,n] > 1);
endfunction

// sopi_isSquare ...............................................................
// tests whether a given sopiVar is square
function isSquare = sopi_isSquare(var)
   [m,n]    = size(var);
   isSquare = m == n;
endfunction

// sopi_testMatrixPositivity ...................................................
// determines whether a symmetric matrix is positive definite, positive
// semidefinite, negative semidefinite, negative definite or undefinite.
function out = sopi_testMatrixPositivity(H)
    if ~sopi_isSymmetric(H) then
        error("The matrix must be symmetric")
    end
    lambdaMin = eigs(H,[],1,"SA");
    if lambdaMin > 0 then
        out = "pd";
    elseif lambdaMin == 0 then
        out = "psd";
    elseif lambdaMin < 0 then
        lambdaMax = eigs(H,[],1,"LA");
        if lambdaMax < 0 then
            out = "nd";
        elseif lambdaMax == 0 then
            out = "nsd";
        else
            out = "i";
        end
    end
endfunction

// sopi_isSymmetric ............................................................
// tests whether a given matrix is symmetric
function out = sopi_isSymmetric(A)
    if norm(A-A',%inf) <%eps then
        out = %t;
    else
        out = %f;
    end
endfunction

// sopi_isInv ..................................................................
// tests whether a given matrix is invertible
function out = sopi_isInv(A)
//   if issparse(A) then
//      //
//      if  argn(2) < 2 then
//         c = condestsp(A,2);
//      else
//         c = condestsp(A,LUp);
//      end
//   else
      c = cond(full(A))
//   end
   if  c < 1/(%eps) then
      out = %t;
   else
      out = %f;
   end
endfunction
