// Copyright 2015 - Pierre Vuillemin
// License CECILL
//

// ============================================================================
// SOPICST CREATION
// ============================================================================

// sopi_newCst .................................................................
// creates an empty sopiCst object.
function constraint = sopi_newCst()
   objType     = "sopiCst";
   objAtt      = list("type",   "",...
                      "class",  [],...
                      "id",     [],...
                      "lhs",    [],...
                      "rhs",    []);
   constraint   = sopi_makeObject(objType,objAtt);
endfunction

// sopi_storeCst ...............................................................
// stores a given constraint in the global sopiNameSpace.
function ptr = sopi_storeCst(newCst)
   ptr = sopi_store("cst",newCst);
endfunction

// ============================================================================
// INEQUALITY CONSTRAINTS
// ============================================================================

// sopiPtr <= b ................................................................
function out = %sopiPtr_3_s(p,maxVal)
   sopi_loadConstants();
   sopi_expectPtrToVar(p);
   //    sopi_expectType(["var";"tmpVar"],p);
   var         = sopi_getVar(p);
   constraint  = sopi_defineConstraint(var,LESSER_THAN,maxVal);
   out         = sopi_storeCst(constraint);
   sopi_print(3,"Creating inequality constraint.\n")
   sopi_cleanTmpVar(p);
endfunction

// b >= sopiPtr ................................................................
function out = %s_4_sopiPtr(maxVal,p)
//   sopi_loadConstants();
   sopi_expectPtrToVar(p)
   out = p <= maxVal;
   //    sopi_cleanTmpVar(p);
endfunction

// sopiPtr >= b ................................................................
function out = %sopiPtr_4_s(p,minVal)
   sopi_loadConstants();
   sopi_expectPtrToVar(p);
   var         = sopi_getVar(p);
   constraint  = sopi_defineConstraint(var,GREATER_THAN,minVal);
   out         = sopi_storeCst(constraint);
   sopi_print(3,"Creating inequality constraint.\n")
//   sopi_cleanTmpVar(p);
endfunction

// b <= sopiPtr ................................................................
function out = %s_3_sopiPtr(minVal,p)
   sopi_expectPtrToVar(p);
   out = p >= minVal;
   //    sopi_cleanTmpVar(p);
endfunction

// sopiPtr <= sopiPtr ..........................................................
function out = %sopiPtr_3_sopiPtr(p1,p2)
   sopi_expectPtrToVar(p1,p2);
   out = (p1-p2) <= 0;
   //    sopi_cleanTmpVar(p1,p2);
endfunction

// sopiPtr >= sopiPtr ..........................................................
function out = %sopiPtr_4_sopiPtr(p1,p2)
   sopi_expectPtrToVar(p1,p2);
   out = (p1 - p2) >= 0;
endfunction

// ============================================================================
// EQUALITY CONSTRAINTS
// ============================================================================

// sopiPtr == sopiPtr ..........................................................
function out = %sopiPtr_o_sopiPtr(p1,p2)
   sopi_expectPtrToVar(p1,p2);
   out = p1-p2 == 0;
endfunction

// sopiPtr == b ................................................................
function out = %sopiPtr_o_s(p,rhs)
   sopi_loadConstants();
   sopi_expectPtrToVar(p)
   sopi_print(3,"Creating equality constraint.\n")
   var         = sopi_getVar(p);
   constraint  = sopi_defineConstraint(var,EQUALS,rhs);
   out         = sopi_storeCst(constraint);
//   sopi_cleanTmpVar(p);
endfunction

// ============================================================================
// TOOLS FUNCTIONS OVERLOADING
// ============================================================================

// disp ........................................................................
function %sopiCst_p(c)
   if c.lhs.isLinear | c.lhs.isCPWA then
      cType = " affine";
   else
      cType = "";
   end
   disp("sopiCst : " +string(size(c.lhs,1)) + cType +" constraints");
endfunction

// extraction ..................................................................
function ans = %sopiCst_e(i,c)
   if typeof(i) == "string" then
      select i
      case "isLinear"
         ans = c.class == 1;
      case "isConvex"
         ans = c.class == 2;
      case "isConcave"
         ans = c.class == -2;
      case "isCPWA"
         ans = c.class == 2 & c.lhs.isCPWA;
      case "isNC"
         ans = c.class == 3;
      end
   else
      error("invalid extraction for constraints")
   end
endfunction

// ============================================================================
// CONSTRAINTS CONCATENATION
// ============================================================================

// list & sopiPtr ..............................................................
function out = %l_h_sopiPtr(cList,p)
   for c = cList
      if ~c.isCst then
         error("Can only concatenate constraints together")
      end
   end
   out      = cList;
   out($+1) = p;
endfunction

// sopiPtr & sopiPtr ...........................................................
function out = %sopiPtr_h_sopiPtr(p1,p2)
   sopi_expectPtrToCst(p1,p2)
   out = list(p1,p2)
endfunction
