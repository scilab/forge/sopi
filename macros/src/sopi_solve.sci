// Copyright 2015 - Pierre Vuillemin
// License CECILL
//

//
// sopi_solve ..................................................................
// is the interface for solving problems
function sopi_solve(pb,paramList)
   if length(paramList)>0 then
      method = paramList(1);
   else
      method = [];
   end
   infoStr = "Nature of the optimisation problem : ";
   select pb.class
   case "lp"
      sopi_print(0,infoStr + "linear.\n")
      [xopt, fopt, info]   = sopi_chooseLPSolver(pb,method);
      fopt                 = fopt + pb.r;
//   case "ilp"
//            sopi_print(0,infoStr + "integer linear\n")
//   case "milp"
//            sopi_print(0,infoStr + "mixed integer linear.\n")
//   case "qp"
//   case "convex"
//   case "nonconvex"
   else
      error("Unknown class")
   end
   sopi_setVar(pb,"xopt",xopt);
   sopi_setVar(pb,"fopt",fopt);
endfunction

// sopi_chooseLPSolver .........................................................
function [xopt, fopt, info] = sopi_chooseLPSolver(pb,method)
   xopt = [];
   fopt = [];
   // Available solvers
   KARMARKAR            = "karmarkar";
   LINPRO               = "linpro";
   SOPILP               = "sopiLP"
   // Default choice
   if isempty(method) then
      method = KARMARKAR;
   end
   sopi_print(0,"Solving problem with : " + method + ".\n")
   //
   select method
   case KARMARKAR // built-in scilab solver
      tic()
      timer()
      [xopt, fopt, flag]   = karmarkar(full(pb.Aeq),full(pb.beq),pb.c,[],[],[],[],[],full(pb.A),full(pb.b),pb.lb,pb.ub);
      info.elapsedTime     = toc();
      info.cpuTime         = timer();
      flagMeaning          = ["1","The algorithm has converged.\n",
                              "0","Maximum number of iterations reached.\n",
                             "-1","No feasible point has been found.\n",
                             "-2","The problem is unbounded.\n"];
      info.vFlag           = ["karmarkar",sopi_interpretFlag(flag,flagMeaning)];
   case LINPRO // from external module quapro
      sopi_testIfInstalled("linpro","To use the solver linpro, the quapro module must be installed. You can get it from the atoms portal.");
      tic()
      timer()
      lb                   = pb.lb;
      ub                   = pb.ub;
      lb(lb == -%inf)      = -number_properties('huge'); // as advised in the help...
      ub(ub == %inf)       = number_properties('huge');
      try
         [xopt, lagr, fopt]   = linpro(full(pb.c), full([pb.Aeq;pb.A]), full([pb.beq;pb.b]), lb, ub, size(pb.Aeq,1));
         info.vFlag           = ["linpro","The algorithm has converged.\n"];
         flag                 = 1;
      catch
         flag                 = -1;
         info.vFlag           = "linpro : " + lasterror();
      end
      info.elapsedTime     = toc();
      info.cpuTime         = timer();
   case SOPILP // sopi built-in
      [xopt, fopt, flag, info] = sopi_lpSolve(pb.c, pb.A, pb.b, pb.Aeq, pb.beq, pb.lb, pb.ub, "primal");
   end
   // Display a warning if the algorithm has not really converged
   if flag == 1
      sopi_print(0,info.vFlag);
   else
      sopi_print(-1,info.vFlag);
   end
endfunction

// sopi_chooseQPSolver .........................................................
function [xopt, fopt, info] = sopi_chooseQPSolver(pb,method)
   // Available solvers
   QUAPRO = "quapro";
   SOPIQP = "sopiQP";
   // Default choice
   if isempty(method) then
      method = QUAPRO;
   end
   select method 
   case QUAPRO
      tic()
      timer()
      try
      //      [xopt, lagr, fopt] = quapro()
   catch
      flag                 = -1;
      info.vFlag           = "linpro : " + lasterror();
      end
      info.elapsedTime  = toc();
      info.cpuTime      = timer();
      info.vFlag        = ""
   case SOPIQP
//      [xopt, fopt, flag, info] = sopi_qpSolve(pb.H, pb.c, pb.A, pb.b, pb.Aeq, pb.beq, pb.lb, pb.ub);
   end
endfunction

// sopi_interpretFlag ..........................................................
function vFlag = sopi_interpretFlag(flag,flagMeaning)
   idx   = find(flag == eval(flagMeaning(:,1)));
   vFlag = flagMeaning(idx,2);
endfunction
