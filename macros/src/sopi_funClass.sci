// Copyright 2015 - Pierre Vuillemin
// License CECILL
//

// sopi_newFunClass ............................................................
function funClass = sopi_newFunClass(fun,m,n)
   // curvature code :
   // 0    : constant
   // 1    : affine
   // -2   : concave
   // 2    : convex
   // 3    : nl
   funClass        = mlist(["sopiFunClass","curvature","type"])
   funClass.type   = [];
   if argn(2)>0 then
      if typeof(fun) == "string" then
         M           = ones(m,n)
         select fun
         case "constant"
            funClass.curvature      = 0*M;
         case "convex"
            funClass.curvature      = 2*M;
         case "affine"
            funClass.curvature      = 1*M;
         case "concave"
            funClass.curvature      = -2*M;
         case "nonconvex"
            funClass.curvature      = 3*M;
         else
            error("unknown function")
         end
      else
         funClass.curvature = fun;
      end

   end
endfunction

// sopi_applyLinearMappingRules ................................................
// determines the new class of function after a linear transformation.
function cout = sopi_applyLinearMappingRules(cin,A)
   x = cin.curvature;
   M = zeros(size(A,1),size(x,2));
   for i = 1:size(A,1)
      for j =1:size(x,2)
         curv = 0;
         for k = 1:size(A,2)
            newTerm = sopi_applyScalarLinearRules(A(i,k),x(k,j));
            curv    = sopi_applyScalarAdditionRules(curv,newTerm);
         end
         M(i,j) = curv;
      end
   end
   cout = sopi_newFunClass(M);
endfunction

// sopi_applyScalarLinearRules .................................................
function cout = sopi_applyScalarLinearRules(a,c)
   if abs(c) == 2 then
      // x convex (concave), a*x is :
      // a > 0 : convex (concave)
      // a < 0 : concave (convex)
      cout = sign(a) * c;
   else
      cout = c;
   end
endfunction

// sopi_applyScalarAdditionRules ...............................................
function cout = sopi_applyScalarAdditionRules(c1,c2)
   if c1 == c2 then
      cout = c1;
   else
      if c1 + c2 ==0 then // convex + concave = non convex
         cout = 3;
      else
         v       = [c1,c2]
         [m, k]  = max(abs(v));
         cout    = sign(v(k))*m;
      end
   end
endfunction

// sopi_applyAdditionRules .....................................................
function cout = sopi_applyAdditionRules(c1,c2)
   C1  = c1.curvature;
   C2  = c2.curvature;
   M   = zeros(size(C1,1),size(C1,2));
   for i = 1:size(C1,1)
      for j = 1:size(C1,2)
         M(i,j) = sopi_applyScalarAdditionRules(C1(i,j),C2(i,j));
      end
   end
   cout = sopi_newFunClass(M);
endfunction

// sopi_applyCompositionRules ..................................................
function c = sopi_applyCompositionRules(argClass,funCurvature,mn)
   if argn(2)<4 then
      funType = [];
   end
   if funCurvature == 2 & and(argClass.curvature == 1) then //f(A*x + b) is convex if f is convex
      c = sopi_newFunClass("convex",mn(1),mn(2));
   elseif funCurvature == -2 & and(argClass.curvature == 1) then
      c = sopi_newFunClass("concave",mn(1),mn(2));
   else
      c = sopi_newFunClass("nonconvex",mn(1),mn(2));
   end
endfunction

// sopi_applyPointwiseMaxRules .................................................
function cout = sopi_applyPointwiseMaxRules(argClass1,argClass2)
   C1  = argClass1.curvature;
   C2  = argClass2.curvature;
   if and(C1 == 1) & and(C2 == 1) then
      cout        = sopi_newFunClass(2);
      //        cout.type   = "pwa"
   elseif and(C1 == 2) & and(C2 == 1) | and(C1 == 1) & and(C2 == 2) then
      cout        = sopi_newFunClass(2);
   elseif and(C1 == 2) & and(C2 == 2) then
      cout        = sopi_newFunClass(2);
   else
      error("pointwise max not yet implemented")
   end
endfunction
