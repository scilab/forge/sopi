// Copyright 2015 - Pierre Vuillemin
// License CECILL
//

// ============================================================================
// SOPIPRO CREATION
// ============================================================================

// sopi_problem ................................................................
// creates an empty problem, store it in the sopi namespace and return the
// associated pointer.
function p = sopi_problem()
   pb  = sopi_newProblem();
   p   = sopi_storePb(pb);
endfunction

// sopi_storePb ................................................................
// adds a given problem to the global list "pb"
function ptr = sopi_storePb(newPb)
   ptr = sopi_store("pb",newPb);
   sopi_addSpecificProFields(ptr)
endfunction

// sopi_addSpecificProFields ...................................................
function sopi_addSpecificProFields(p)
   funStart = "id = "+string(p.id)+";ptr = sopi_newPtr(""pb"",id);";
   deff("foo(name)",funStart + "sopi_setVar(ptr,""name"",name);");
   sopi_setVar(p,"setName",foo);
   deff("foo2(varargin)",funStart + "sopi_addVarToPb(ptr,varargin);");
   sopi_setVar(p,"addVar",foo2);
   deff("foo3(fun)",funStart + "sopi_addFunToPb(ptr,fun);");
   sopi_setVar(p,"addFun",foo3);
   //    deff("foo4(nNewVar)",funStart + "sopi_updatePbConstraint(ptr,nNewVar);");
   //    sopi_setVar(p,"updateConstraint",foo4);
   deff("[out] = foo4(varPtr)",funStart + "out = sopi_getVarIdxInPb(ptr,varPtr);");
   sopi_setVar(p,"getVarIdx",foo4);
   deff("foo5(varargin)",funStart + "sopi_addConstraintToPb(ptr,varargin);");
   sopi_setVar(p,"addConstraint",foo5);
   deff("foo6(varargin)",funStart + "sopi_solve(ptr,varargin);");
   sopi_setVar(p,"solve",foo6);
   deff("[out] = foo7(varargin)",funStart + "out = sopi_getOptValue(ptr,varargin);");
   sopi_setVar(p,"getOptValue",foo7);
endfunction

// sopi_newProblem .............................................................
// creates an empty optimisation problem
function pb = sopi_newProblem()
   objType  = "sopiPro";
   objAtt   = list("id"       ,     [],...
                   "class"    ,     "",...
                   "name"     ,     "",...
                   "varList"  , list(),...
                   "varIdx"   , list(),...
                   "intIdx"   , list(),...
                   "binIdx"   , list(),...
                   "lb"       ,     [],...
                   "ub"       ,     [],...
                   "H"        ,     [],...
                   "c"        ,     [],...
                   "r"        ,     [],...
                   "A"        ,     [],...
                   "b"        ,     [],...
                   "Aeq"      ,     [],...
                   "beq"      ,     [],...
                   "xopt"     ,     [],...
                   "fopt"     ,     [],...
                   "cType"    ,     [],...
                   "addVar"   ,     [],...
                   "addConstraint", [],...
                   "setName"  ,     [],...
                   "addFun"   ,     [],...
                   "getVarIdx",     [],...
                   "solve"    ,     [],...
                   "getOptValue",   [])
   pb       = sopi_makeObject(objType,objAtt);
endfunction

// ============================================================================
// SOPIPRO SPECIFIC FUNCTIONS
// ============================================================================

// sopi_addFunToPb .............................................................
// parses a given function to translate it into a standard optimisation problem.
function sopi_addFunToPb(p,fun)
   varList = fun.dependsOf;
   p.addVar(varList);
   if fun.isLinear then
      [costList, varList, constant]    = sopi_parseLinearFun(fun)
      c                                = p.c;
      for i = 1:length(costList)
         idx                           = p.getVarIdx(varList(i));
         c(idx)                        = c(idx) + costList(i);
      end
      r                                = p.r;
      r                                = r + constant;
      sopi_setVar(p,"c",c)
      sopi_setVar(p,"r",r)
   elseif fun.isConvex then
      if fun.isCPWA then
         [newFun, newCst] = sopi_convexToEpigraph(fun)
         sopi_addConstraintToPb(p,newCst);
         sopi_addFunToPb(p,newFun);
      else
         error("general convex functions are not yet handled")
      end
   else
      error("only linear or convex pwa functions are handled.")
   end
endfunction

// sopi_parseLinearFun .........................................................
function [costList,varList,constant] = sopi_parseLinearFun(var)
   costList        = list();
   varList         = list();
   constant        = 0;
   if var.isElementary then
      costList(1) = ones(size(var,1),1);
      varList(1)  = var; 
   else
      lastTransform   = var.child.operator;
      select lastTransform
      case LEFT_LINEAR_MAPPING
         if var.child.arg(2).isElementary then
            costList(1) = var.child.arg(1);
            varList(1)  = var.child.arg(2);
            constant    = 0;
         else
            [costList,varList,constant] = sopi_parseLinearFun(var.child.arg(2))
            for i = 1:length(costList)
               costList(i) = var.child.arg(1) * costList(i);
            end
            constant = var.child.arg(1) * constant;
         end
         // TODO : RIGHT_LINEAR_MAPPING
      case CONSTANT
         constant    = var.child.arg(1);
      case SUM
         for i = 1:length(var.child.arg)
            [ci, vari, bi] = sopi_parseLinearFun(var.child.arg(i));
            if ~isempty(ci) then
               costList($+1)   = ci(:);
               varList($+1)    = vari(:);
            end
            constant            = constant + bi;
         end
      else
         disp("else case in parse Linear fun");
         costList(1) = 1;
         varList(1)  = var;
      end
   end
endfunction

// sopi_convexToEpigraph .......................................................
// transforms a convex objective function f into a linear one by introducing a 
// slack variable t constrained to lie in the epigraph (i.e. above) of the 
// convex fonction through the constraint f(x) <= t.
function [newFun,newConstraints] = sopi_convexToEpigraph(fun)
   [m,n]          = size(fun);
   slackVar       = sopi_var(m,n); // slackVar is in the epigraph of the function
   c1             = slackVar >= 0;
   c2             = funPtr <= slackVar;
   newFun         = sopi_getVar(slackVar);
   newConstraints = c1 & c2;
endfunction

// sopi_addVarToPb .............................................................
// adds the variables given in a list to the optimisation problem if those
// variables are not already present.
function sopi_addVarToPb(p,newVarList)
   varList        = p.varList;
   idxList        = p.varIdx;
   isIntegerList  = p.intIdx;
   isBinaryList   = p.binIdx;
   newBounds      = list();
   for var = newVarList
      if isempty(sopi_findVarInList(var,varList)) then
         // if the variable is not already in the variable list of the problem
         varList($+1)   = var;
         nvar           = prod(size(var));
         sopi_increasePbNvar(p,nvar);
         if length(idxList) > 0 then
            offset = idxList($)($);
         else
            offset = 0;
         end
         idxList($+1) = offset + 1 : offset + nvar;
         if var.isInteger  then
            // if the variable is integer, its indexes are stored in a list
            isIntegerList($+1)   = idxList($);
         end
         if var.isBinary then
            // same thing if the variable is binary
            isBinaryList($+1)    = idxList($);
         end
      end
   end
   // Update of the problem with the new lists
   sopi_setVar(p,"intIdx",isIntegerList);
   sopi_setVar(p,"binIdx",isBinaryList)
   sopi_setVar(p,"varList",varList);
   sopi_setVar(p,"varIdx",idxList);
endfunction

// sopi_findVarInList ..........................................................
// find the position of var in the list varList by comparing the id of the 
// variables.
function pos = sopi_findVarInList(var,varList)
   pos = [];
   for j = 1:length(varList)
      if var.id == varList(j).id
         pos = j;
      end
   end
endfunction

// sopi_getVarIdxInPb ..........................................................
function out = sopi_getVarIdxInPb(p,varPtr)
   pos = sopi_findVarInList(varPtr,p.varList);
   if isempty(pos) then
      out = [];
   else
      out = p.varIdx(pos);
   end
endfunction

// sopi_increasePbNvar .........................................................
// adjusts the sizes of the various matrices involved in the optimisation problem
// to be coherent with the new size
function sopi_increasePbNvar(p,nNewVar)
   if isempty(p.A) then
      A = sparse([],[],[0,nNewVar]);
   else
      A = sopi_addColumns(p.A,nNewVar);
   end
   if isempty(p.Aeq) then
      Aeq = sparse([],[],[0,nNewVar]);
   else
      Aeq = sopi_addColumns(p.Aeq,nNewVar);
   end
   if isempty(p.c) then
      c = zeros(nNewVar,1);
   else
      c = sopi_addColumns(p.c',nNewVar)';
   end
   if isempty(p.H) then
      H = sparse([],[],[nNewVar,nNewVar]);
   else
      H = sysdiag(p.H,sparse([],[],[nNewVar,nNewVar]));
   end
   if isempty(p.ub) then
      ub = %inf*ones(nNewVar,1);
   else
      ub = [p.ub;%inf*ones(nNewVar,1)];
   end
   if isempty(p.lb) then
      lb = -%inf*ones(nNewVar,1);
   else
      lb = [p.lb;-%inf*ones(nNewVar,1)];
   end
   sopi_setVar(p,"H",H);
   sopi_setVar(p,"A",A);
   sopi_setVar(p,"Aeq",Aeq);
   sopi_setVar(p,"c",c);
   sopi_setVar(p,"lb",lb);
   sopi_setVar(p,"ub",ub);
endfunction

// sopi_addColumns .............................................................
// adds empty columns to a given matrix in a sparse or dense way depending on the
// nature of the input matrix.
function newA = sopi_addColumns(A,ncol)
   if issparse(A) then
      M = sparse([],[],[size(A,1),ncol]);
   else
      M = zeros(size(A,1),ncol);
   end
   newA = [A,M];
endfunction

// sopi_addConstraintToPb ......................................................
// is the main interface for adding constraints to an optimisation problem.
function sopi_addConstraintToPb(p,cList)
   for c = cList
      if c.isLinear then
         sopi_updateConstraintsTypes(p,"lin");
         sopi_addLinearConstraintToPb(p,c);
      elseif c.isCPWA then
         [newLhs,newC]     = sopi_CPWAConstraintToLinearConstraint(c.lhs);
         newC($+1)         = sopi_defineConstraint(newLhs,LESSER_THAN,c.rhs);
//         sopi_addVarToPb(p,newVar);
         sopi_addConstraintToPb(p,newC);
      else
         error("only liner constraints")
      end
   end
endfunction

// sopi_updateConstraintsTypes .................................................
function sopi_updateConstraintsTypes(p,cType)
   cTypeList = p.cType;
   cTypeList = unique([cTypeList,cType]);
   sopi_setVar(p,"cType",cTypeList);
endfunction

// sopi_CPWAConstraintToLinearConstraint .......................................
// convertes convex piecewise affine constraints to linear constraints.
function [newVar, newC] = sopi_CPWAConstraintToLinearConstraint(var)
   if var.child.operator == LEFT_LINEAR_MAPPING then
      [newV,newC]   = sopi_CPWAConstraintToLinearConstraint(var.child.arg(2))
      newVar        = var.child.arg(1) * newV;
   elseif var.child.operator == SUM then
      newVar   = [];
      newC     = list();
      for i = 1:length(var.child.arg)
         [newVi,newCi]  =  sopi_CPWAConstraintToLinearConstraint(var.child.arg(i));
         //            newFi           = sopi_retrievePtr(newFi);
         newVar         = newVar + newVi;
         newC           = lstcat(newC,newCi);
      end
   elseif var.child.operator == FUN then
      funName = var.child.arg(1);
      select funName
      case ABS
         [m,n]          = size(var);
         slackVarPtr    = sopi_var(m,n);
         // TODO : ATTENTION SI VAR NEST PAS UN VECTEUR...NE PAS INTERPRETER COMME SDP
         [newV, newC]   = sopi_CPWAConstraintToLinearConstraint(var.child.arg(2));
         varPtr         = sopi_retrievePtr(newV); // TODO: faire qqch pour ça...
         c1             = varPtr <= slackVarPtr;
         varPtr         = sopi_retrievePtr(newV);
         c2             = varPtr >= - slackVarPtr;
         c3             = slackVarPtr >= 0;
         newVar         = sopi_getVar(slackVarPtr);
         newC           = lstcat(c1 & c2 & c3, newC);
      case MAX
         slackVarPtr    = sopi_var(1);
         newC           = list(slackVarPtr >= 0);
         newVar         = sopi_getVar(slackVarPtr);
         for i = 1:length(var.child.arg(2))
            [newVi, newCi]    = sopi_CPWAConstraintToLinearConstraint(var.child.arg(2)(i));
            newC              = lstcat(newC,newCi);
            varPtr            = sopi_retrievePtr(newVi); // TODO : and that...
            newC($+1)         = varPtr <= slackVar;
//            newVar            = lstcat(newVar,newVi);
         end
      case NORM
         normType = var.child.arg(3);
         if normType == 1 then
            [m, n]          = size(var.child.arg(2));
         elseif normType == %inf then
            m               = 1;
         end
         slackVarPtr       = sopi_var(m);
         varPtr            = sopi_retrievePtr(var.child.arg(2));// TODO : and that...
         c1                = varPtr <= slackVarPtr;
         varPtr            = sopi_retrievePtr(var.child.arg(2)); // TODO : and that...
         c2                = varPtr >= -slackVarPtr;
         c3                = slackVarPtr >= 0;
         newVar            = sopi_getVar(slackVarPtr);
         newC              = c1 & c2 & c3;
      end
   else
      newVar   = var;
      newC     = list();
   end
   
endfunction

// sopi_addLinearConstraintToPb ................................................
// adds linear constraints to a given problem
function sopi_addLinearConstraintToPb(p,c)
   var = c.lhs.dependsOf;
   p.addVar(var);
   if c.lhs.isElementary then // bound
      if c.type == GREATER_THAN then
         LB          = p.lb;
         varIdx      = p.getVarIdx(var);
         LB(varIdx)  = c.rhs;
         sopi_setVar(p,"lb",LB);
      elseif c.type == LESSER_THAN then
         UB          = p.ub;
         varIdx      = p.getVarIdx(var);
         UB(varIdx)  = c.rhs;
         sopi_setVar(p,"ub",UB);
      end
   else
      [ai,bi]     = sopi_extractLinearConstraintMatrices(p,c.lhs);
      rhs         = c.rhs-bi;
      if c.type == EQUALS then
         Aeq     = [p.Aeq;ai];
         beq     = [p.beq;rhs];
         sopi_setVar(p,"Aeq",Aeq);
         sopi_setVar(p,"beq",beq);
      elseif c.type == LESSER_THAN then
         A       = [p.A;ai];
         b       = [p.b;rhs];
         sopi_setVar(p,"A",A);
         sopi_setVar(p,"b",b);
      elseif c.type == GREATER_THAN then
         A       = [p.A;-ai];
         b       = [p.b;-rhs];
         sopi_setVar(p,"A",A);
         sopi_setVar(p,"b",b);
      end
   end
endfunction

// sopi_extractLinearConstraintMatrices ........................................
// determines A & b forming a linear constraint from the graph of 
// transformations contained in the lhs of a constraint.
function [A,b] = sopi_extractLinearConstraintMatrices(p,var)
   //
   A = [];
   b = [];
   if var.isElementary then
      nvar        = p.getNvar;
      A           = sparse([],[],[size(var,1),nvar]);

      idxVar      = p.getVarIdx(var);
      A(:,idxVar) = speye(size(var,1),size(var,1));
   else
      select var.child.operator
      case LEFT_LINEAR_MAPPING
         A1      = var.child.arg(1);
         [A2,b]  = sopi_extractLinearConstraintMatrices(p,var.child.arg(2));
         A       = A1*A2;
         b       = A1*b;
      case CONSTANT
         b       = var.child.arg(1);
      case SUM
         for i = 1:length(var.child.arg)
            [Ai,bi]     = sopi_extractLinearConstraintMatrices(p,var.child.arg(i));
            b           = b + bi;
            A           = A + Ai;
         end
      else
         error("else case")
      end
   end
endfunction

// sopi_getOptValue ............................................................
// extracts the optimal values of the user defined optimisation variables.
function out = sopi_getOptValue(p,varList)
   if isempty(varList) then
      out     = list();
      [ptrList,ptrName] = sopi_whoIsSopiPtr();
      for i = 1:length(ptrList)
         if ptrList(i).isVar then
            optValue = sopi_getOptValue(p,list(ptrList(i)));
            if ~isempty(optValue) then
               out($+1) = list(ptrName(i),optValue)
            end
         end
      end
   else
      out = list();
      for var = varList
         idx         = p.getVarIdx(var);
         out($+1)    = p.xopt(idx);
      end
      if length(varList) == 1 then
         out         = out(:);
      end
   end
endfunction

// ============================================================================
// SOPIPRO OVERLOADING
// ============================================================================

// disp ........................................................................
// TODO : proper display of sopiPro
function %sopiPro_p(pb)
   disp("this is a sopiPro")
endfunction

// extraction ..................................................................
function out = %sopiPro_e(i,pb)
   if typeof(i) == "string" then
      if i == "getNvar" then
         out = pb.varIdx($)($);
      else
         error("invalid field name for sopiPro")
      end
   else
      error("invalid field for sopiPro extraction")
   end
endfunction
