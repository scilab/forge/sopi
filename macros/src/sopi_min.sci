// Copyright 2015 - Pierre Vuillemin
// License CECILL
//

// ============================================================================
// FILLING THE SOPIPRO OBJECT
// ============================================================================

// sop_min .....................................................................
// is the main interface for problem instanciation
function pb = sopi_min(funPtr,varargin)
   if ~isscalar(funPtr) then
      error("Multi-objective optimisation is not handled.")
   end
   sopi_loadConstants();
   // Reading constraints
   constraintList = list();
   if sopi_constraintsAreGlobal() then
      if length(varargin)>0 then
         disp("Global constraints mode activated, discarding input constraints")
      end
      constraintList = sopi_getAllConstraints();
   else
      for i = 1:length(varargin)
         if typeof(varargin(i)) == "sopiPtr" then
            constraintList($+1) = varargin(i);
         elseif typeof(varargin(i)) == "list" then
            constraintList = lstcat(constraintList,varargin(i));
         else
            error("Unexpected input")
         end
      end
   end
   // Optimisation problem construction
   pb      = sopi_problem();
   pb.addConstraint(constraintList);
   pb.addFun(funPtr);
   // add upper and lower bounds for binary variables
   if ~isempty(pb.binIdx) then
      // TODO : add a warning for incompatible bounds
      lb          = pb.lb;
      binIdx      = list2vect(pb.binIdx);
      lb(binIdx)  = 0;
      ub          = pb.ub;
      ub(binIdx)  = 1;
      sopi_setVar(pb,"lb",lb);
      sopi_setVar(pb,"ub",ub);
   end
   // Determining the type of optimisation problem
   // TODO : Test type of problem before processing? as it can change the way some constraints are handled?
   if pb.cType == "lin"  & funPtr.isLinear | funPtr.isCPWA then
      if isempty(pb.intIdx) & isempty(pb.binIdx) then
         sopi_setVar(pb,"class","lp")
      elseif length(pb.intIdx) + length(pb.binIdx) ==  length(pb.varList) then
         sopi_setVar(pb,"class","ilp");
      else
         sopi_setVar(pb,"class","milp");
      end
      //   elseif pb.cType == "lin" & funPtr.isQuadratic then
      //      sopi_setVar(pb,"class","qp")
   end
endfunction

// sopi_minimise ...............................................................
function sopi_minimise(funPtr,varargin)
   if argn(2)<2 then
      cList    = [];
      method   = [];
   else
      if typeof(varargin($)) == "string" then
         method  = varargin($);
         if length(varargin)>1
            cList   = varargin(1:$-1)
         else
            cList = [];
         end
      else
         cList    = varargin;
         method   = [];
      end
   end
   if ~isempty(cList) then
      problem = sopi_min(funPtr,cList);
   else
      problem = sopi_min(funPtr);
   end
   clear funPtr
   if ~isempty(method) then
      execstr("problem.solve("""+method+""");");
   else
      execstr("problem.solve();");
   end

   execstr("xopt    = problem.getOptValue();");
   out     = [];
   arg     = [];
   for i = 1:length(xopt)
      out = [out,xopt(i)(1)];
      arg = [arg,"["+strcat(string(xopt(i)(2)),";")+"]"];
   end
   exStr = "[" + strcat(out,',') + "] = resume(" + strcat(arg,",")+");";
   execstr(exStr);
endfunction
