// Copyright 2015 - Pierre Vuillemin
// License CECILL
//

// sopi_solveUNLP ..............................................................
// solves an unconstrained non-linear optimisation problem.
function [xopt, fopt, flag, info] = sopi_solveUNLP(fun, x0, method, opt)
   tic();
   timer();
   // Optional arguments
   if argn(2) < 3 then
      method  = "lineSearch";
      opt     = [];
   elseif argn(2) < 4 then
      opt     = [];
   end
   // Options for the optimisation
   defaultOptions  = mlist(["sopiOptions","grad","hess","descentDir","lineSearchAlg","maxIter","tolx","tolf","tolg"],...
                                              %f,    %f,      "bfgs", "strong-wolfe",      100,  1e-8,  1e-8, 1e-8);
   options         = sopi_parseOptimOptions(opt,defaultOptions);
   // Retrieve fun name
   varList = who_user(%f);
   for i = 2:size(varList,1)
      varName = varList(i);
      execstr("var = " + varName);
      if typeof(var) == "function" then
         if var == fun & isdef(varName,"n") then
            funName = varName;
            break;
         end
      end
   end
   // Renaming
   if funName == "fun" then
      funToBeOptimised  = evstr(funName);
      funName           = "funToBeOptimised";
   end
   // Wrap the objective function to include numerical derivatives
   if ~options.grad & ~options.hess then
      deff("[f, g, h] = wrappedFun(x)",["nargout = argn(1);";
                                       "if nargout >= 1 then";
                                       "f = "+funName+"(x);";
                                       "end"
                                       "if nargout == 2 then";
                                       "g = numderivative("+funName+",x);";
                                       "end";
                                       "if nargout == 3 then";
                                       "[g, h] = numderivative("+funName+",x);";
                                       "end"]);
   elseif options.grad & ~options.hess
      deff("[f, g, h] = wrappedFun(x)",["nargout = argn(1);";
                                       "if nargout == 1 then";
                                       "f = "+funName+"(x);";
                                       "end"
                                       "if nargout >= 2 then";
                                       "[f, g] = "+funName+"(x);";
                                       "end";
                                       "if nargout == 3 then";
                                       "[trash,h] = numderivative("+funName+",x);";
                                       "end"]);
   else
      wrappedFun = fun;
   end
   //
   select method
   case "lineSearch"
      [xopt, fopt, flag, info] = sopi_descentMethod(wrappedFun, x0, options);
      //    case "trust-region"
      //        [xopt, fopt, flag] = sopi_trustRegionMethod(fun, x0, options);
   else
      error("Unknown descent method name");
   end
   info.elapsedTime  = toc();
   info.cpuTime      = timer();
   clear wrappedFun;
endfunction

// sopi_descentMethod ..........................................................
// performs an unconstrained optimisation of a given function by relying on a
// line-search in a descent direction at each iteration.
function [xopt, fopt, flag, info] = sopi_descentMethod(fun, x0, options)
   //
   xk                  = x0;
   xkm1                = [];
   fkm1                = %inf;
   gkm1                = [];
   gk                  = [];
   Hkm1                = [];
   //
   k                   = 0;
   hasConverged        = %f;
   while ~hasConverged
      k                       = k+1
      // Get a descent direction
      [dk, fk, gk, Hk]        = sopi_getDescentDirection(fun, xkm1, xk, gkm1, Hkm1, options.descentDir);
      // Test
      [hasConverged, flag]    = sopi_testConvergence(xk, xkm1, fk, fkm1, gk, k, options);
      if hasConverged
         break;
      end
      // Get a step length in the direction dk
      ak                      = sopi_getStepLength(fun, xk, fk, gk, dk, options.lineSearchAlg);
      // Nex iterate
      xkp1                    = xk + ak * dk;
      //
      xkm1                    = xk;
      xk                      = xkp1;
      fkm1                    = fk;
//      fk                      = fkp1
      gkm1                    = gk;
//      gk                      = gkp1
   end
   xopt        = xk;
   fopt        = fk;
   info.niter  = k;
//   info.nfeval  = ;
endfunction

// sopi_getStepLength ..........................................................
// computes a step-length in a given direction based on a given method.
function ak = sopi_getStepLength(fun, xk, fk, gk, dk, method)
   select method
   case "strong-wolfe"
      ak = sopi_getStrongWolfeStep(fun, xk, fk, gk, dk);
   case "backtracking-armijo"
      // TODO :
   else
      error("unknown line search method")
   end
endfunction

// sopi_getStrongWolfeStep .....................................................
// looks for a step-length which satisfies the strong Wolfe conditions.
// This algorithm comes from the book : 
// Fletcher. Practical methods for optimization. (2000).
function alpha = sopi_getStrongWolfeStep(fun, xk, fk, gk, dk)
   // Algorithm parameters
   t1 = 9;
   t2 = 0.1;
   t3 = 0.5;
   c1 = 0.1;
   c2 = 0.9;
   //
   akm1     = 0
   phi0     = fk;
   dphi0    = dk'*gk;
   phiakm1  = phi0
   ak       = 1;
   // Line search
   admissibleIntervalFound = %f;
   stepLengthFound         = %f;
   // 1. Bracketing phase
   admissibleInterval      = [];
   while ~admissibleIntervalFound
      xkp1           = xk + ak * dk;
      [phiak, gkp1]  = fun(xkp1);
      //
      xIsAboveLine   = phiak > phi0 + c1*ak*dphi0;
      objIncreased   = phiak >= phiakm1;
      if xIsAboveLine | objIncreased then
         admissibleInterval      = [akm1, ak];
         admissibleIntervalFound = %t;
      else
         dphiak = dk'*gkp1;
         if abs(dphiak) <= -c2*dphi0 then 
            // The candidate step length satisfies strong-Wolfe conditions
            alpha             = ak;
            stepLengthFound   = %t;
            break;
         end
         if dphiak >= 0 then
            // the objectif function increases in the direction dk at the point 
            // xpk1, hence, the current interval is admissible
            admissibleInterval      = [akm1, ak];
            admissibleIntervalFound = %t;
            break;
         end
         // Modify the interval
         phiakm1  = phiak;
         I        = [2*ak - akm1, ak + t1*(ak-akm1)];
         akm1     = ak;
         ak       = sopi_selectPointInInterval(fun, I, xk, dk)
      end
   end
   // 2. Sectioning phase
   lb = min(admissibleInterval);
   ub = max(admissibleInterval);
   while ~stepLengthFound
      a              = lb + t2*(ub - lb);
      b              = ub - t3*(ub - lb);
      ak             = sopi_selectPointInInterval(fun, [a, b], xk, dk)
      xa             = xk + a * dk;
      xkp1           = xk + ak * dk;
      phia           = fun(xa);
      [phiak, gkp1]  = fun(xkp1);
      //
      xIsAboveLine   = phiak > phi0 + c1*ak*dphi0;
      objIncreased   = phiak >= phia;
      if xIsAboveLine | objIncreased then
         ub = ak;
         lb = a;
      else
         dphiak = dk'*gkp1;
         if abs(dphiak) <= -c2*dphi0 then 
            // The candidate step length satisfies strong-Wolfe conditions
            alpha             = ak;
            stepLengthFound   = %t;
         else
            if (b-a) * dphiak >=0 then
               lb = a;
               ub = ak;
            else
               lb = ak;
               ub = b;
            end
         end
      end

      if abs(ub-lb) <= %eps then
         error("upper bound and lower bound too close")
      end

   end
endfunction

// sopi_selectPointInInterval ..................................................
// selects a point in a given interval by performing a quadratic interpolation 
// of the function fun in the direction d over the interval and returns the 
// point which minimises the quadratic interpolation.
function alpha = sopi_selectPointInInterval(fun, I, x, d)
   if or(isnan(I)) then
      error("NAN in candidate interval")
   end
   a = min(I(1));
   b = max(I(2));
   if a ~= b then
      // Quadratic interpolation
      c        = a+b/2;
      fa       = fun(x + a*d);
      fc       = fun(x + c*d);
      fb       = fun(x + b*d);
      //
      beta12   = a^2 - c^2;
      gamma12  = a - c;
      beta23   = c^2 - b^2;
      gamma23  = c - b;
      beta31   = b^2 - a^2;
      gamma31  = b - a;
      alpha    = 1/2*(beta23*fa+ beta31*fc+ beta12*fb)/(gamma23*fa+gamma31*fc+gamma12*fb);
      if isnan(alpha) then
         error("alpha is nan in interpolation process")
      end
   else
      alpha = a;
   end
endfunction

// sopi_getDescentDirection ....................................................
// returns a descent direction corresponding to a given method.
function [dk, fk, gk, Hk] = sopi_getDescentDirection(fun, xkm1, xk, gkm1, Hkm1, method)
   select method
   case "steepest"
      // Steepest descent
      [fk, gk]    = fun(xk);
      dk          =  - gk;
      Hk          = []
   case "bfgs"
      [fk, gk]  = fun(xk);
      if isempty(gkm1) then // first iteration
         Hk = speye(length(xk),length(xk));
         dk = -gk;
      else
         Hk = sopi_inverseHessianEstimation(xkm1, xk, gkm1, gk, Hkm1);
         dk = -Hk*gk;
      end
   case "newton"
      // Newton iteration
      [fk, gk, Hk]   = fun(xk);
      dk             = - Hk \ gk;
   end
endfunction

// sopi_testConvergence ........................................................
// tests the convergence of an unconstrained optimisation algorithm by 
// comparing various quantities to given tolerances.
function [ans,flag] = sopi_testConvergence(xkm1, xk, fk, fkm1, gk, k, opt)
   xDiff = norm(xk - xkm1,%inf);
   if fkm1<%inf then
      fDiff = norm(fk - fkm1,%inf);
   else
      fDiff = %inf;
   end
   gNorm = norm(gk, %inf);
   if xDiff <= opt.tolx then
      ans   = %t;
      flag  = sprintf("Decrease of x below tolerance : %.2e < %.2e",xDiff,opt.tolx);
   elseif fDiff <= opt.tolf
      ans = %t;
      flag = sprintf("Decrease of f below tolerance : %.2e < %.2e",fDiff,opt.tolf);
   elseif k == opt.maxIter
      ans   = %t;
      flag  = sprintf("Maximum number of iterations reached : %d",opt.maxIter);
   elseif gNorm <= opt.tolg
      ans = %t;
      flag = sprintf("Gradient norm below tolerance")
   else
      ans   = %f;
      flag  = "";
   end
endfunction

// sopi_inverseHessianEstimation ...............................................
// computes the estimation of the inverse of the hessian matrix through the 
// inverse BFGS update.
function iBk = sopi_inverseHessianEstimation(xkm1, xk, gkm1, gk, iBkm1)
   s   = xk - xkm1;
   y   = gk - gkm1;
   if y'*s ~= 0 then
      v   = speye(length(xk),length(xk)) - (y*s')/(y'*s);
      iBk = v'*iBkm1*v+ (s*s')/(y'*s);
   else
      iBk  = iBkm1;
   end
endfunction
